# SUDOKU #

UQAC – Université du Québec à Chicoutimi
8INF846-01 - Intelligence artificielle - Hiver 2022
Travail Pratique #2 - Création CSP pour le jeu du Sudoku

### Groupe 01 ### 

* Gabriel Shenouda
* Marcelo Vasconcellos
* Matthis Villeneuve
* Yann Reynaud

### What is this repository for? ###

* Creating the Development Environment
* Running the sudoku

### 1. Creating the Development Environment ###

Inside the folder run the following command to create the virtualenv.

```python -m venv venv```

To activate the virtual environment run:

* On Windows

    ```.\venv\Scripts\activate```

* On Mac/Linux

    ```source venv\bin\activate```

It is advisable that we upgrade pip tool:

```python -m pip install --upgrade pip```

Install the requirements:.

```pip install -r requirements.txt```

For deactivate virtualenv:

```deactivate```

### 2. Running the sudoku ###

Run the comment below in the terminal, it will open the browser.

```python main.py```