import random
import numpy as np

NB_ITER = 0


def inc_nb_iter():
    """
    Obligé de faire ça pour modifier une variable globale en python
    """
    global NB_ITER
    NB_ITER += 1


class Plateau:

    def __init__(self):
        self.plateau = [[0 for i in range(9)] for j in range(9)]

    def display(self):
        for y in range(len(self.plateau[0])):
            if y % 3 == 0 and y != 0:
                print("---------------------")
            for x in range(len(self.plateau[1])):
                if x % 3 == 0 and x != 0:
                    print("|", end=" ")
                    print(self.plateau[y][x] or ' ', end=" ")
                elif x == len(self.plateau[1]) - 1:
                    print(self.plateau[y][x] or ' ')
                else:
                    print(self.plateau[y][x] or ' ', end=" ")

    def get_plateau(self):
        return self.plateau

    def get_col_values(self, row_nb, col_nb):
        col_list = []
        for row in range(0, 9):
            if self.plateau[row][col_nb] != 0 and row != row_nb:
                col_list.append(self.plateau[row][col_nb])
        return col_list

    def get_row_values(self, row_nb, col_nb):
        row_list = []
        for col in range(0, 9):
            if self.plateau[row_nb][col] != 0 and col != col_nb:
                row_list.append(self.plateau[row_nb][col])
        return row_list

    def get_square_values(self, row_nb, col_nb):
        square_list = []
        row_start = row_nb - (row_nb % 3)
        col_start = col_nb - (col_nb % 3)
        for row in range(row_start, row_start + 3):
            for col in range(col_start, col_start + 3):
                # if self.plateau[row][col] != 0 and row != row_nb and col != col_nb:
                if self.plateau[row][col] != 0 and tuple([row, col]) != tuple([row_nb, col_nb]):
                    square_list.append(self.plateau[row][col])
        return square_list

    def create_table_with_txt_file(self):
        with open("sudoku.txt", 'r') as file:
            content = file.read()
            # print(content)
        j = 0
        compteur = 0
        for k in range(0, len(content)):
            if content[k] == "\n":
                j += 1
                compteur += 1
            else:
                self.plateau[j][(k - compteur) % 9] = int(content[k])
        return self.plateau

    def creation_sudoku(self):

        # plateau = [
        #     [7, 8, 0, 4, 0, 0, 1, 2, 0],
        #     [6, 0, 0, 0, 7, 5, 0, 0, 9],
        #     [0, 0, 0, 6, 0, 1, 0, 7, 8],
        #     [0, 0, 7, 0, 4, 0, 2, 6, 0],
        #     [0, 0, 1, 0, 5, 0, 9, 3, 0],
        #     [9, 0, 4, 0, 6, 0, 0, 0, 5],
        #     [0, 7, 0, 3, 0, 0, 0, 1, 2],
        #     [1, 2, 0, 0, 0, 7, 4, 0, 0],
        #     [0, 4, 9, 2, 0, 6, 0, 0, 7], ]

        # self.plateau = [
        #     [8, 0, 0, 0, 0, 6, 7, 0, 0],
        #     [0, 3, 0, 0, 0, 0, 5, 0, 0],
        #     [0, 7, 0, 2, 4, 0, 0, 0, 0],
        #     [2, 0, 0, 0, 8, 0, 6, 0, 1],
        #     [4, 0, 0, 0, 5, 0, 0, 0, 2],
        #     [0, 0, 0, 6, 0, 0, 0, 0, 5],
        #     [0, 0, 0, 0, 0, 8, 0, 0, 0],
        #     [0, 6, 0, 1, 0, 0, 0, 0, 0],
        #     [5, 0, 0, 0, 0, 9, 1, 3, 4], ]

        row_nb = 0
        for row in self.plateau:
            col_nb = 0
            for col in self.plateau[row_nb]:
                if random.choices([True, False, False, False, False, False, ]):
                    number_choice = random.choice(range(9))
                    not_null_square = [n for n in self.get_square_values(row_nb, col_nb) if n > 0]
                    if len(not_null_square) < 2 and \
                            number_choice not in self.get_col_values(row_nb, col_nb) and \
                            number_choice not in self.get_row_values(row_nb, col_nb) and \
                            number_choice not in self.get_square_values(row_nb, col_nb):
                        self.plateau[row_nb][col_nb] = number_choice
                col_nb += 1
            row_nb += 1
        return self.plateau


class Game:
    def __init__(self, plateau: Plateau):
        self.plateau = plateau
        self.vide = []
        for row in range(9):
            for col in range(9):
                if self.plateau.plateau[row][col] == 0:
                    self.vide.append((row, col))
        self.history = []
        # print(self.vide)
        # positions qui ne sont pas remplies

    def positions_vides(self):
        self.vide.clear()
        for row in range(9):
            for col in range(9):
                if self.plateau.plateau[row][col] == 0:
                    self.vide.append((row, col))

    # def est_valide(self, row, col,
    #               val):  # on doit vérifier 3 choses, unicité dans la col, la row, et le carré
    #     for ind in range(9):
    #         if self.plateau[row][ind] == val and ind != col:  # on vérifie row et col
    #             print("row issue")
    #             return False
    #         if self.plateau[ind][col] == val and ind != row:
    #             print("col issue")
    #             return False
    #
    #     carre_l = row - (row % 3)  # coordonnées row du début du carré dans lequel on est
    #     carre_c = col - (col % 3)  # coordonnées col du début du carré dans lequel on est
    #     for l in range(carre_l, carre_l + 3):
    #         for c in range(carre_c, carre_c + 3):
    #             if self.plateau[l][c] == val and row != l and col != c:
    #                 print("carré issue")
    #                 return False
    #     print("c bon")
    #     return True

    def constraints(self, row_nb, col_nb):
        col = self.plateau.get_col_values(row_nb, col_nb)
        row = self.plateau.get_row_values(row_nb, col_nb)
        square = self.plateau.get_square_values(row_nb, col_nb)
        return row + col + square

    def possibilities(self, row_nb, col_nb):
        contraints = self.constraints(row_nb, col_nb)
        g_list = []
        for k in range(1, 10):
            # list(set()) enlève les doublons
            if k not in list(set(contraints)):
                g_list.append(k)
        return g_list

    def mrv(self):
        """
        en entrée la liste des cases vides, et avoir en sortie quelle case choisir
        et quel est son domaine de valeur (quelles valeurs lui assigner)
        """
        save_index = []
        save_min = np.inf
        save_max = -np.inf
        final_index = np.inf
        final_row = 0
        final_col = 0
        # Finding the index of cases which have less possibilities

        for index in range(len(self.vide)):
            row_nb = self.vide[index][0]
            col_nb = self.vide[index][1]
            if len(self.possibilities(row_nb, col_nb)) < save_min:
                save_index.clear()
                save_index.append(index)
                save_min = len(self.possibilities(row_nb, col_nb))
            elif len(self.possibilities(row_nb, col_nb)) == save_min:
                save_index.append(index)

        for k in range(len(save_index)):
            row_nb = self.vide[save_index[k]][0]
            col_nb = self.vide[save_index[k]][1]
            if len(self.constraints(row_nb, col_nb)) >= save_max:
                save_max = len(self.constraints(row_nb, col_nb))
                final_index = save_index[k]
                final_row = row_nb
                final_col = col_nb

        return final_row, final_col

    def back_tracking(self):
        print()
        print('=' * 21)
        print()
        print("Départ :")
        self.plateau.display()
        if not self.recursive_back_tracking():
            print("Impossible de trouver une solution")
        pass

    def recursive_back_tracking(self):

        inc_nb_iter()
        # on veut ici donner en entrée la liste des cases vides, et avoir en sortie quelle case choisir
        # et quel est son domaine de valeur (quelles valeurs lui assigner)

        # met à jour self.vides
        self.positions_vides()

        if len(self.vide) <= 0:
            print()
            print('=' * 21)
            print()
            print("Résolu")
            print("Réalisé en :", NB_ITER, " coups")
            print()
            self.plateau.display()
            return True

        print('=' * 21)
        print()
        print("Nombre d'essai :", NB_ITER)
        print()

        # print("ind" ,ind)
        self.plateau.display()

        # nous donne la case et les valeurs à essayer d'après le MRV
        row, col = self.mrv()
        values = self.possibilities(row, col)

        for val in values:
            self.plateau.plateau[row][col] = val

            # si cest valide on passe à la case dapres
            if self.recursive_back_tracking():
                return True

        self.plateau.plateau[row][col] = 0
        return False


if __name__ == "__main__":
    print("""
    UQAC – Université du Québec à Chicoutimi
    8INF846-01 - Intelligence artificielle - Hiver 2022
    Travail Pratique #2 - Création CSP pour le jeu du Sudoku
    
    Groupe 01:
    * Gabriel Shenouda
    * Marcelo Vasconcellos
    * Matthis Villeneuve
    * Yann Reynaud
    """)
    message = """
    Comment voulez-vous commencer le sudoku ? (1/2)
    
    1 - Générer automatiquement une table de sudoku;
    2 - Permet à l'utilisateur d'informer la table initiale via sudoku.txt.
    
    Resp. """
    execute_method = input(message)
    while execute_method not in ('1', '2'):
        execute_method = input(message)

    if execute_method == '1':
        plateau = Plateau()
        plateau.creation_sudoku()
        game = Game(plateau)
        game.back_tracking()

    elif execute_method == '2':
        plateau = Plateau()
        plateau.create_table_with_txt_file()
        game = Game(plateau)
        game.back_tracking()
